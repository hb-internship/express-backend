import {Application} from "express";
import passport from "passport";
import { Strategy } from "passport-local";

import {IUser} from "./schemes/interfaces/user.interface";
import User from "./schemes/user.model";

passport.use(new Strategy(
async (username: string, password: string, done: (err: Error | null, user: IUser | boolean | null) => any) => {
    try {
        const user = await User.findOne({ username }).select("+password");
        if (!user) {
            return done(null, false);
        }
        const valid = user.comparePassword(password);
        if (!valid) {
            return done(null, false);
        }
        user.set("password", undefined, { strict: false });
        return done(null, user);
    } catch (err) {
        // tslint:disable-next-line:no-console
        console.error(err);
        return done(err, null);
    }
}));

import passportJWT, { ExtractJwt } from "passport-jwt";
const JWTStrategy = passportJWT.Strategy;

passport.use(new JWTStrategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: "hugo-boss-test"
}, (jwtPayload: IUser, done: (e: Error | null, user: IUser | boolean | null) => void) => {
    return User.findById(jwtPayload.id).then((user) => {
        if (!user) {
            return done(null, false);
        }
        return done(null , user);
    }).catch((err: Error) => {
        return done(err, null);
    });
}));

export default function(app: Application) {
    app.use(passport.initialize());
}
