import {IProduct} from "../schemes/interfaces/product.interface";

class ProductsCache {
    private accessTimes: any;
    private cache: any;
    private entries: number;
    private readonly maxEntries: number;

    constructor() {
        this.cache = {};
        this.accessTimes = {};
        this.entries = 0;
        this.maxEntries = 32;
    }

    public containsId(id: string): boolean {
        return this.cache.hasOwnProperty(id);
    }

    public removeLRU(): void {
        const ids = Object.keys(this.accessTimes);
        let min = { id: "", age: Number.POSITIVE_INFINITY};
        for (const id of ids) {
            const age = this.accessTimes[id];
            if (age < min.age) {
                min = { id, age };
            }
        }
        this.clearProduct(min.id);
        return;
    }

    public getProduct(id: string): IProduct {
        if (!this.containsId(id)) {
            throw new Error("Non existing entry");
        }
        this.accessTimes[id] = Date.now();
        return this.cache[id];
    }

    public setProduct(product: IProduct): void {
        const { _id: id } = product;
        if (!this.containsId(id)) {
            if (this.entries === this.maxEntries) {
                this.removeLRU();
            }
            this.entries++;
        }
        this.accessTimes[id] = Date.now();
        this.cache[id] = product;
    }

    public clearProduct(id: string): void {
        delete this.accessTimes[id];
        delete this.cache[id];
        this.entries--;
    }

    public clearAllEntries(): void {
        this.cache = {};
        this.accessTimes = {};
        this.entries = 0;
    }
}

export default ProductsCache;
