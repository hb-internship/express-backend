import {NextFunction, Request, Response} from "express";
import HttpException from "../exceptions/HttpException";
import {IUser} from "../schemes/interfaces/user.interface";

import ProductsCache from "./cache.class";

export const adminCheck = (req: Request, res: Response, next: NextFunction) => {
    const user: IUser = req.user as IUser;
    const { isAdmin } = user;
    if (!req.user || !isAdmin) {
        return res.status(401).end();
    }
    return next();
};

export const errorHandler = (e: HttpException, req: Request, res: Response, next: NextFunction) => {
    if (e && !e.status) {
        // tslint:disable-next-line:no-console
        console.error(e);
        return res.status(500).json(e);
    }
    return res.status(e.status).json({ error: e, message: e.message, status: e.status });
};

export const productsCache = new ProductsCache();
