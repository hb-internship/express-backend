import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import express, {Application, NextFunction, Request, Response} from "express";

import logger from "morgan";
import path from "path";

import HttpException from "./exceptions/HttpException";

import PassportInit from "./passport";
import RouterInit from "./routes";

import mongoose, {Connection} from "mongoose";
import {errorHandler} from "./utils";

const app: Application = express();

mongoose.connect("mongodb://localhost/hb-bistro-test", {
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const db: Connection = mongoose.connection;
// tslint:disable-next-line:no-console
db.on("error", console.error.bind(console, "Connection error:"));
db.once("open", () => {
    // tslint:disable-next-line:no-console
    console.log("Successfully connected to MongoDB");
});

// view engine setup
__dirname += "/..";

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(bodyParser.urlencoded({ extended: false }));

PassportInit(app);
RouterInit(app);

// catch 404 and forward to error handler
app.use((req: Request, res: Response, next: NextFunction) => {
    next(new HttpException(404, "Page not found"));
});

// error handler
app.use(errorHandler);

export default app;
