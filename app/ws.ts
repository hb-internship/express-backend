import http from "http";
import * as WebSocket from "ws";

let instance: WebSocket.Server;

export function wsBroadcastData(data: any) {
    if (typeof data !== "string") {
        data = JSON.stringify(data);
    }
    if (!instance) {
        throw new Error("Invalid WS server");
    }
    instance.clients.forEach((client) => {
       client.send(data);
    });
}

export default function(server?: http.Server): WebSocket.Server {
    if (!server && !instance) {
        throw new Error("Attempted to retrieve undefined WS server");
    }
    if (!instance) {
        instance = new WebSocket.Server({server});
    }
    return instance;
}
