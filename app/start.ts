#!/usr/bin/env node

/**
 * Module dependencies.
 */

import debugModule from "debug";
import http from "http";
import * as WebSocket from "ws";

import app from "./app";
import getWsServer from "./ws";

const debug = debugModule("demo:server");

/**
 * Get port from environment and store in Express.
 */

const port = normalizePort(process.env.PORT || "3000");
app.set("port", port);

/**
 * Create HTTP server.
 */

const server: http.Server = http.createServer(app);

/**
 * Create WS server.
 */

const wss: WebSocket.Server = getWsServer(server);

wss.on("connection", (ws: WebSocket) => {
    ws.on("message", (message: string) => {
        ws.send(JSON.stringify({ message: `Hello, you sent -> ${message}`}));
    });
    ws.send(JSON.stringify({ message: `"Hi there, I am a WebSocket server"`}));
});

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on("error", onError);
server.on("listening", onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val: string) {
    const p = parseInt(val, 10);

    if (isNaN(p)) {
        // named pipe
        return val;
    }

    if (p >= 0) {
        // p number
        return p;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

interface IHttpServerError extends Error {
    syscall: string;
    code: string;
}

function onError(error: IHttpServerError) {
    if (error.syscall !== "listen") {
        throw error;
    }

    const bind = typeof port === "string"
        ? "Pipe " + port
        : "Port " + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case "EACCES":
            // tslint:disable-next-line:no-console
            console.error(bind + " requires elevated privileges");
            process.exit(1);
            break;
        case "EADDRINUSE":
            // tslint:disable-next-line:no-console
            console.error(bind + " is already in use");
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    const addr = server.address();
    if (!addr) {
        return debug("Address was invalid");
    }
    const bind = typeof addr === "string"
        ? "pipe " + addr
        : "port " + addr.port;
    debug("Listening on " + bind);
}
