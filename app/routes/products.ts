import express, {NextFunction, Request, Response, Router} from "express";
const router: Router = express.Router();

import passport from "passport";

import {IProduct} from "../schemes/interfaces/product.interface";

import Product from "../schemes/product.model";
import {adminCheck} from "../utils";

const pageSize = 25;

router.get("/", async (req: Request, res: Response, next: NextFunction) => {
    const { page } = req.query || 0;

    try {
        const count: number = await Product.countDocuments({ visible: true });
        const products: IProduct[] = await Product.find({ visible: true } )
            .skip(page * pageSize)
            .limit(pageSize)
            .sort({ createdAt: -1 });
        return res.status(200).json({ products, pageSize, count });
    } catch (e) {
        return next(e);
    }
});

router.get("/search", async (req: Request, res: Response, next: NextFunction) => {
    const { q } = req.query;
    if (!q) {
        return res.status(200).json({ products: [] });
    }
    try {
        const re = new RegExp(`.*${q}.*`, "gi");
        const products: IProduct[] = await Product.find({ name: re });
        return res.status(200).json({ products, count: products.length });
    } catch (e) {
        return next(e);
    }
});

router.get("/category/:catId", async (req: Request, res: Response, next: NextFunction) => {
    try {
        const products: IProduct[] = await Product.find({ categories: req.params.catId });
        return res.status(200).json({ products, count: products.length});
    } catch (e) {
        return next(e);
    }
});

router.use(passport.authenticate("jwt", { session: false }));

router.use(adminCheck);

router.get("/all", async (req: Request, res: Response, next: NextFunction) => {
    const { page } = req.query || 0;
    try {
        const count: number = await Product.countDocuments({});
        const products: IProduct[] = await Product.find({} )
            .skip(page * pageSize)
            .limit(pageSize)
            .sort({ createdAt: -1 });
        return res.status(200).json({ products, pageSize, count });
    } catch (e) {
        return next(e);
    }
});

router.post("/new", async (req: Request, res: Response, next: NextFunction) => {
    // tslint:disable-next-line:no-console
    if (req.body.cost) {
        req.body.cost = parseFloat(req.body.cost);
    }
    try {
        const product: IProduct = await (new Product(req.body)).save();
        return res.status(201).json({ product });
    } catch (e) {
        return next(e);
    }
});

router.put("/:id", async (req: Request, res: Response, next: NextFunction) => {
    const { id } = req.params;
    if (req.body.cost) {
        req.body.cost = Math.round(parseFloat(req.body.cost) * 100) / 100;
    }
    try {
        const product = await Product.findByIdAndUpdate(id, req.body, { new: true });
        return res.status(200).json({ product });
    } catch (e) {
        return next(e);
    }
});

router.delete("/:id", async (req: Request, res: Response, next: NextFunction) => {
    const { id } = req.params;
    try {
        const product = await Product.findByIdAndDelete(id);
        return res.status(200).json({ product });
    } catch (e) {
        return next(e);
    }
});

export default router;
