import express, {NextFunction, Request, Response, Router} from "express";
const router: Router = express.Router();

import passport from "passport";

import Basket from "../schemes/basket.model";
import Product from "../schemes/product.model";
import User from "../schemes/user.model";

import HttpNotFoundException from "../exceptions/HttpNotFoundException";
import HttpUnauthorizedException from "../exceptions/HttpUnauthorizedException";
import {IBasket} from "../schemes/interfaces/basket.interface";
import {IUser} from "../schemes/interfaces/user.interface";

import HttpException from "../exceptions/HttpException";
import {adminCheck, productsCache} from "../utils";

import {IProduct} from "../schemes/interfaces/product.interface";
import { wsBroadcastData } from "../ws";

const fetchProducts = async (products: [string]) => {
    const fetchProduct = async (id: string) => {
        if (productsCache.containsId(id)) {
            return productsCache.getProduct(id);
        } else {
            const product = await Product.findById(id);
            if (!product) {
                throw new Error("Product not found");
            }
            productsCache.setProduct(product);
            return product;
        }
    };

    const result: { [ key: string ]: IProduct } = {};
    for (const p of products) {
        result[p] = await fetchProduct(p);
    }

    return result;
};

const processOrder = async (body: { order: [{ count: number, id: string}] }) => {
    let cost = 0;
    const order: { [key: string]: number } = {};
    for (const entry of body.order) {
        const { count, id } = entry;
        if (productsCache.containsId(id)) {
            cost += productsCache.getProduct(id).cost * count;
        } else {
            const product = await Product.findById(id);
            if (!product) {
                continue;
            }
            cost += product.cost * count;
            productsCache.setProduct(product);
        }
        order[id] = count;
    }

    return {
        cost,
        order
    };
};

async function commitOrder(user: IUser | null, basket: IBasket, cost: number) {
    if (!user) {
        throw new HttpUnauthorizedException();
    }
    const products = await fetchProducts(basket.products);
    wsBroadcastData({
        basket,
        products,
        user
    });
    user = await User.findByIdAndUpdate(user._id, { balance: user.balance - cost }, { new: true });
    return { basket, cost, user };
}

router.post("/", async (req: Request, res: Response, next: NextFunction) => {
    if (!req.body || !req.body.order) {
        return next(new HttpException(400, "Could not process order"));
    }
    try {
        let user;
        if (req.body.badgeId) {
            user = await User.findOne({ badgeId: req.body.badgeId });
            if (!user) {
                return next(new HttpNotFoundException("Contact support for further assistance"));
            }
        } else {
            return next(new HttpUnauthorizedException("There was a problem with the badge id"));
        }
        const { order, cost } = await processOrder(req.body);
        const basket: IBasket = await (new Basket({
            cost,
            order,
            products: Object.keys(order),
            user: user._id,
        }).save());
        if (user.isGuest && user.balance - cost < 0) {
            await Basket.findByIdAndDelete(basket._id);
            return res.status(403).json({ message: "Insufficient funds, please recharge your card" });
        }
        return res.status(201).json(await commitOrder(user, basket, cost));
    } catch (e) {
        return next(e);
    }
});

router.use(passport.authenticate("jwt", { session: false }));

router.get("/user", async (req: Request, res: Response, next: NextFunction) => {
    const user = req.user as IUser;
    const page = parseInt(req.query.page, 10) || 0;
    const size = parseInt(req.query.size, 10) || pageSize;
    if (!user) {
        return next(new HttpUnauthorizedException("Invalid user"));
    }
    try {
        const count = await Basket.countDocuments({ user: user._id });
        const baskets: IBasket[] = await Basket.find({ user: user._id })
            .sort({ createdAt: -1 })
            .limit(size)
            .skip(page * size);
        let products = {};
        for (const basket of baskets) {
            const p = await fetchProducts(basket.products);
            products = Object.assign(products, p);
        }
        return res.status(200).json({
            baskets,
            count,
            pageSize: (size || pageSize),
            products
        });
    } catch (e) {
        return next(e);
    }
});

router.post("/employees", async (req: Request, res: Response, next: NextFunction) => {
    if (!req.body || !req.body.order) {
        return next(new HttpException(400, "Could not process order"));
    }
    if (!req.user) {
        return next(new HttpNotFoundException("Contact support for further assistance"));
    }

    const user = req.user as IUser;

    try {
        const {order, cost} = await processOrder(req.body);
        const basket: IBasket = await (new Basket({user: user._id, order, products: Object.keys(order), cost}).save());
        return res.status(201).json(await commitOrder(user, basket, cost));
    } catch (e) {
        next(e);
    }
});

router.get("/", async (req: Request, res: Response, next: NextFunction) => {
    try {
        const m = parseInt(req.query.m, 10);
        const minutesAgo = new Date(Date.now() - (m * 60 * 1000));
        const baskets: IBasket[] = await Basket.find({ createdAt: { $gte: minutesAgo } }).populate("user");
        let products = {};
        for (const basket of baskets) {
            const p = await fetchProducts(basket.products);
            products = Object.assign(products, p);
        }
        return res.status(200).json({ baskets, products, count: baskets.length });
    } catch (e) {
        return next(e);
    }
});

router.use(adminCheck);

const pageSize = 25;

const parseDateRangeCondition = (start: Date, end: Date, userId?: string): object  => {
    const user = userId ? { user: userId } : {};
    return Object.assign({
        $and: [
            {
                createdAt: { $gte: new Date(start) }
            }, {
                createdAt: { $lte: new Date(end) }
            }
        ]
    }, user);
};

router.get("/all", async (req: Request, res: Response, next: NextFunction) => {
    const page = parseInt(req.query.page, 10) || 0;
    const { start, end } = req.query;
    try {
        const condition = parseDateRangeCondition(start, end);
        const count: number = await Basket.countDocuments(condition);
        const baskets: IBasket[] = await Basket.find(condition)
            .populate("user")
            .sort({ createdAt: -1 })
            .limit(pageSize)
            .skip(pageSize * page);
        return res.status(200).json({ baskets, page, pageSize, count });
    } catch (e) {
        return next(e);
    }
});

router.get("/all/:userId", async (req: Request, res: Response, next: NextFunction) => {
    const page = parseInt(req.query.page, 10) || 0;
    const { userId } = req.params;
    const { start, end } = req.query;
    try {
        const condition = parseDateRangeCondition(start, end, userId);
        const count: number = await Basket.countDocuments(condition);
        const baskets: IBasket[] = await Basket.find(condition)
            .sort({ createdAt: -1 })
            .limit(pageSize)
            .skip(pageSize * page);
        return res.status(200).json({ baskets, page, pageSize, count });
    } catch (e) {
        return next(e);
    }
});

export default router;
