import express, {NextFunction, Request, Response, Router} from "express";
const router: Router = express.Router();

import jwt from "jsonwebtoken";
import passport from "passport";
import {IUser} from "../schemes/interfaces/user.interface";

export default router;
