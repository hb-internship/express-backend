import cors from "cors";

import {Application} from "express";
import adminRouter from "./admin";
import categoriesRouter from "./categories";
import iconsRouter from "./icons";
import ordersRouter from "./orders";
import productsRouter from "./products";
import rootRouter from "./root";
import usersRouter from "./users";

export default (app: Application) => {
  app.use(cors());
  app.use("/api/users", usersRouter);
  app.use("/api/admin", adminRouter);
  app.use("/api/orders", ordersRouter);
  app.use("/api/products", productsRouter);
  app.use("/api/categories", categoriesRouter);
  app.use("/api/icons", iconsRouter);
  app.use("/", rootRouter);
};
