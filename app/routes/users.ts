import express, {NextFunction, Request, Response, Router} from "express";
const router: Router = express.Router();

import jwt from "jsonwebtoken";
import passport from "passport";
import {IUser} from "../schemes/interfaces/user.interface";
import User from "../schemes/user.model";

import HttpException from "../exceptions/HttpException";
import HttpNotFoundException from "../exceptions/HttpNotFoundException";
import HttpUnauthorizedException from "../exceptions/HttpUnauthorizedException";
import {adminCheck} from "../utils";

const pageSize = 25;

const computeFilter = (type: string) => {
    switch (type) {
        case "employee": return { isAdmin: false, isGuest: false };
        case "admin": return { isAdmin: true };
        case "guest": return { isGuest: true };
        default: return {};
    }
};

const parseSort = (type: string) => {
    if (!type || !([" ", "-"].includes(type[0]))) {
        return {};
    }
    return {
        [type.slice(1)]: type[0] === "-" ? -1 : 1
    };
};

router.post("/login", (req: Request, res: Response, next: NextFunction) => {
    passport.authenticate("local", { session: false }, (err: Error, user: IUser, info: object) => {
        if (err || !user) {
            return next(err || new HttpUnauthorizedException("Invalid credentials"));
        }

        req.login(user, { session: false }, (e) => {
            if (e) {
                return res.send(e);
            }

            if (!user) {
                return res.status(401).json({ message: "Invalid credentials" });
            }

            const {username, _id: id} = user;
            const token = jwt.sign({
                id,
                username
            }, "hugo-boss-test", {
                expiresIn: "30 days"
            });
            return res.json({ user, token });
        });
    })(req, res);
});

router.get("/login",
    passport.authenticate("jwt", { session: false}),
    async (req: Request, res: Response, next: NextFunction) => {
        res.status(200).end();
});

router.use(passport.authenticate("jwt", { session: false }));

router.get("/auth-check", (req: Request, res: Response, next: NextFunction) => {
    res.status(200).end();
});

router.get("/profile", async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { user } = req;
        res.status(200).json({ user });
    } catch (e) {
        return next(e);
    }
});

router.get("/logout", (req: Request, res: Response) => {
    req.logout();
    res.redirect("/");
});

router.use(adminCheck);

router.get("/stats", async (req: Request, res: Response, next: NextFunction) => {
    try {
        const usersCount = (await User.countDocuments({}));
        const guestCount = (await User.countDocuments({ isGuest: true }));
        const adminCount = (await User.countDocuments({ isAdmin: true }));
        return res.status(200).json({ pageSize, usersCount, guestCount, adminCount });
    } catch (e) {
        return next(e);
    }
});

router.get("/check", async (req: Request, res: Response, next: NextFunction) => {
   try {
       let user;
       user = await(User.findOne({email: req.query.email}));
       if (user) {
           return res.status(200).json({ email: true, username: user.username === req.query.username });
       }
       user = await(User.findOne({username: req.query.username}));
       return res.status(200).json({ email: false, username: Boolean(user) });
   } catch (e) {
       next(e);
   }
});

router.get("/group/:type", async (req: Request, res: Response, next: NextFunction) => {
    const page = parseInt(req.query.page, 10);
    const offset = page * pageSize;
    try {
        const users = await User.find(computeFilter(req.params.type))
            .sort(parseSort(req.query.sort))
            .skip(offset)
            .limit(pageSize);
        res.status(200).json({
            count: pageSize,
            users
        });
    } catch (e) {
        res.status(e.status || 400).json(e);
    }
});

const computeSearchFilter = (query: string) => {
    const re = new RegExp(`.*${query}.*`);
    return {
        $or: [
            { firstName: re },
            { lastName: re },
            { username: re},
            { email: re },
            { badgeId: re }
        ]
    };
};

router.get("/search", async (req: Request, res: Response, next: NextFunction) => {
    try {
        const users = await User.find(computeSearchFilter(req.query.q)).limit(pageSize);
        res.status(200).json({
            count: pageSize,
            users
        });
    } catch (e) {
        next(e);
    }
});

router.post("/", async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user = await (new User(req.body).save());
        res.status(201).json({ user });
    } catch (e) {
        next(e);
    }
});

router.get("/:userId", async (req: Request, res: Response, next: NextFunction) => {
    const { userId } = req.params;
    if (!userId) {
        return next(new HttpException(400, "Invalid query"));
    }
    try {
        const user: IUser | null = await User.findById(userId);
        if (!user) {
            return next(new HttpNotFoundException("User not found"));
        }
        return res.status(200).json({ user });
    } catch (e) {
        return next(e);
    }
});

router.put("/:userId", async (req: Request, res: Response, next: NextFunction) => {
   const { userId } = req.params;
   for (const k in req.body) {
       if (!req.body.hasOwnProperty(k)) {
           continue;
       }
       if (["firstName", "lastName", "username", "email"].includes(k)) {
           req.body[k] = req.body[k].toLowerCase();
       }
   }
   try {
       const user = await User.findByIdAndUpdate(userId, req.body, { new: true });
       return res.status(200).json({ user });
   } catch (e) {
       next(e);
   }
});

router.delete("/:userId", async (req: Request, res: Response, next: NextFunction) => {
    const { userId } = req.params;
    try {
        const user = await User.findByIdAndDelete(userId);
        res.status(200).json({ user });
    } catch (e) {
        next(e);
    }
});

router.post("/:userId/badge", async (req: Request, res: Response, next: NextFunction) => {
    const { userId } = req.params;
    const { badgeId } = req.body;
    try {
        await User.updateMany({ badgeId }, { badgeId: "" });
        const user = await User.findByIdAndUpdate(userId, { badgeId }, { new: true });
        return res.status(200).json({ user });
    } catch (e) {
        next(e);
    }
});

export default router;
