import express, { NextFunction, Request, Response, Router } from "express";
const router: Router = express.Router();

import { adminCheck } from "../utils";

import passport from "passport";

import fs from "fs";
import path from "path";
import util from "util";

import Icon from "../schemes/icon.model";

import formidable from "express-formidable";
import HttpNotFoundException from "../exceptions/HttpNotFoundException";
import {IIcon} from "../schemes/interfaces/icon.interface";

const unlink = util.promisify(fs.unlink);

router.get("/all", async (req: Request, res: Response, next: NextFunction) => {
    try {
        const icons: IIcon[] = await Icon.find({});
        res.status(200).json({ icons, count: icons.length });
    } catch (e) {
        return next(e);
    }
});

router.use(passport.authenticate("jwt", { session: false }));

router.use(adminCheck);

router.post("/new", formidable({
    encoding: "utf-8",
    hash: "md5",
    keepExtensions: true,
    uploadDir: "./public/icons/",
}), async (req: Request, res: Response, next: NextFunction) => {
    // @ts-ignore
    const { icon: { path: filepath, type, hash } } = req.files;

    const match: IIcon | null = await Icon.findOne({ hash });
    if (match) {
        try {
            await unlink(filepath);
        } catch (e) {
            // tslint:disable-next-line:no-console
            console.error("Failed to removed duplicated");
        }
        return res.status(200).json(match);
    }

    try {
        const dbIcon: IIcon = await (new Icon({filepath, hash, type})).save();
        return res.status(201).json(dbIcon);
    } catch (e) {
        return next(e);
    }
});

router.delete("/:id", async (req: Request, res: Response, next: NextFunction) => {
    const { id } = req.params;
    try {
        const icon: IIcon | null = await Icon.findByIdAndDelete(id);
        if (!icon) {
            return next(new HttpNotFoundException());
        }
        await unlink(path.join(__dirname, "../..", icon.filepath));
        return res.status(200).json(icon);
    } catch (e) {
        return next(e);
    }
});

export default router;
