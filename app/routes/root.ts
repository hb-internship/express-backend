import express, {NextFunction, Request, Response, Router} from "express";
const router: Router = express.Router();

import path from "path";

/* GET home page. */
router.get("/*", (req: Request, res: Response, next: NextFunction) => {
    return res.sendFile(path.join(__dirname, "../..", "public", "index.html"));
});

export default router;
