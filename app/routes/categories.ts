import express, { NextFunction, Request, Response, Router } from "express";
const router: Router = express.Router();

import {adminCheck} from "../utils";

import passport from "passport";

import HttpNotFoundException from "../exceptions/HttpNotFoundException";

import Category from "../schemes/category.model";

import {ICategory} from "../schemes/interfaces/category.interface";

router.get("/", async (req: Request, res: Response, next: NextFunction) => {
    try {
        const categories: ICategory[] = await Category.find({});
        return res.status(200).json({ categories, count: categories.length });
    } catch (e) {
        return next(e);
    }
});

router.use(passport.authenticate("jwt", { session: false }));

router.use(adminCheck);

router.post("/new", async (req: Request, res: Response, next: NextFunction) => {
    try {
        const category: ICategory = await (new Category({ name: req.body.categoryName.toLowerCase() }).save());
        return res.status(201).json(category);
    } catch (e) {
        return next(e);
    }
});

router.put("/:id", async (req: Request, res: Response, next: NextFunction) => {
   const { id } = req.params;
   try {
       const category: ICategory | null = await Category.findByIdAndUpdate(id, req.body, { new: true });
       if (!category) {
           return next(new HttpNotFoundException());
       }
       return res.status(200).json(category);
   } catch (e) {
       return next(e);
   }

});

router.delete("/:id", async (req: Request, res: Response, next: NextFunction) => {
    const { id } = req.params;
    try {
        const category: ICategory | null = await Category.findByIdAndDelete(id);
        if (!category) {
            return next(new HttpNotFoundException());
        }
        return res.status(200).json(category);
    } catch (e) {
        return next(e);
    }
});

export default router;
