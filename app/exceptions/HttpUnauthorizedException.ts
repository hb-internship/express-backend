import HttpException from "./HttpException";

class HttpUnauthorizedException extends HttpException {
    constructor(message: string = "") {
        super(401, message);
    }
}

export default HttpUnauthorizedException;
