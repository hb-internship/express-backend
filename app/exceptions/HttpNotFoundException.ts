import HttpException from "./HttpException";

class HttpNotFoundException extends HttpException {
    constructor(message: string = "") {
        super(404, message);
    }
}

export default HttpNotFoundException;
