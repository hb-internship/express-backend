import mongoose, {Model, Schema} from "mongoose";
import Category from "./category.model";
import {IProduct} from "./interfaces/product.interface";

const ProductSchema: Schema = new Schema({
    categories: [{ type: Schema.Types.ObjectId, ref: "Category" }],
    cost: { type: Number, required: true },
    icon: { type: Schema.Types.ObjectId, ref: "Icon" },
    name: { type: String, required: true, unique: true },
    visible: { type: Boolean, default: false }
});

// @ts-ignore
ProductSchema.pre(/^find/,  function(this: IProduct) {
    this.populate("categories");
    this.populate("icon");
});

ProductSchema.post("save", async (prod, next) => {
    await prod.populate("categories").execPopulate();
    await prod.populate("icon").execPopulate();
    next();
});

export default mongoose.model<IProduct>("Product", ProductSchema);
