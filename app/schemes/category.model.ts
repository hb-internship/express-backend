import mongoose, { Schema } from "mongoose";
import {ICategory} from "./interfaces/category.interface";

const CategorySchema: Schema = new Schema({
    name: { type: String, unique: true, required: true }
});

export default mongoose.model<ICategory>("Category", CategorySchema);
