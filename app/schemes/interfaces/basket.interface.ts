import {Document} from "mongoose";
import {IProduct} from "./product.interface";
import {IUser} from "./user.interface";

export interface IBasket extends Document {
    cost: number;
    user: IUser["_id"];
    order: {
        [key: string]: number
    };
    products: [IProduct["_id"]];
}
