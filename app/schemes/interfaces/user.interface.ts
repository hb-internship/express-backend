import {Document} from "mongoose";

export interface IUser extends Document {
    username: string;
    email: string;
    firstName: string;
    lastName: string;
    password: string;
    balance: number;
    isAdmin: boolean;
    isGuest: boolean;
    badgeId: string;

    comparePassword: (plain: string) => boolean;
    validateEmail: (email: string) => boolean;
}
