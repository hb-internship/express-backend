import {Document} from "mongoose";

export interface ICategory extends Document {
    name: string;
    products_count: number;
}
