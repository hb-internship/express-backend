import {Document} from "mongoose";

import {ICategory} from "./category.interface";
import {IIcon} from "./icon.interface";

export interface IProduct extends Document {
    icon: IIcon["_id"];
    name: string;
    cost: number;
    categories: Array<ICategory["_id"]>;
    visible: boolean;
}
