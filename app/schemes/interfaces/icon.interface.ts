import {Document} from "mongoose";

export interface IIcon extends Document {
    type: string;
    hash: string;
    filepath: string;
}
