import mongoose, {Document, Schema} from "mongoose";
import {IBasket} from "./interfaces/basket.interface";

const BasketSchema: Schema = new Schema({
   cost: Number,
   order: {
      of: Number,
      required: true,
      type: Map,
   },
   products: [{ type: Schema.Types.ObjectId, ref: "Product"}],
   user: { type: Schema.Types.ObjectId, ref: "User", required: true},
}, {
   timestamps: true
});

export default mongoose.model<IBasket>("Basket", BasketSchema);
