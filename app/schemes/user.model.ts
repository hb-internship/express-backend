import util from "util";

import mongoose, {Document, Query, Schema} from "mongoose";
import {IUser} from "./interfaces/user.interface";

import Bcrypt from "bcryptjs";
import {NextFunction} from "express";

const UserSchema: Schema = new Schema({
    badgeId: { type: String, default: "", unique: true, select: false },
    balance: { type: Number, default: 0.0 },
    email: { type: String, required: true, unique: true },
    firstName: { type: String },
    isAdmin: { type: Boolean, default: false },
    isGuest: { type: Boolean, default: false },
    lastName: { type: String },
    password: { type: String, required: true, select: false },
    username: { type: String, required: true}
}, {
    timestamps: true,
    toJSON: {
        virtuals: true
    },
    toObject: {
        virtuals: true
    },
});

UserSchema.virtual("fullName").get(function(this: IUser) {
   return `${this.lastName} ${this.firstName}`;
});

UserSchema.pre("save", function(this: IUser, next: NextFunction) {
    this.firstName = this.firstName.toLowerCase();
    this.lastName = this.lastName.toLowerCase();
    if (!this.isModified("password")) {
        return next();
    }
    this.password = Bcrypt.hashSync(this.password, 10);
    next();
});

UserSchema.methods.validateEmail = (email: string) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};

UserSchema.methods.comparePassword = function(plain: string) {
    return Bcrypt.compareSync(plain, this.password);
};

export default mongoose.model<IUser>("User", UserSchema);
