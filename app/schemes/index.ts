import Basket from "./basket.model";
import Category from "./category.model";
import Icon from "./icon.model";
import Product from "./product.model";
import User from "./user.model";

export default {
    Basket,
    Category,
    Icon,
    Product,
    User
};
