import mongoose, { Schema } from "mongoose";
import {IIcon} from "./interfaces/icon.interface";

const IconSchema: Schema = new Schema({
   filepath: { type: String, required: true, unique: true },
   hash: { type: String, required: true, unique: true, select: false },
   type: { type: String, required: true }
});

export default mongoose.model<IIcon>("Icon", IconSchema);
