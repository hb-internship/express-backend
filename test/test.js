import mongoose from "mongoose";
import User, { IUser } from '../build/schemes/user.model';

import mock_data from "./mockUsers";

jest.unmock("mongoose");

describe('User model', () => {
  beforeAll(async () => {
    await mongoose.connect("mongodb://localhost/hb-bistro-test", {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
  });
  
  afterAll(async (done) => {
    await mongoose.connection.db.dropDatabase(() => mongoose.disconnect(done));
  });
  
  it("Should save a user", async () => {
    expect.assertions(3);
    const user = new User(mock_data[0]);
    const spy = jest.spyOn(user, 'save');
    await user.save();
    expect(spy).toHaveBeenCalled();
    
    expect(user).toMatchObject({
      firstName: expect.any(String),
      lastName: expect.any(String),
      email: expect.any(String)
    });
    
    expect(user.email).toBe(mock_data[0].email);
  });
  
  it("Should hash the password of the user", async () => {
    const user = await (new User(mock_data[1])).save();
    expect(user.password).not.toBe(mock_data[1].password);
  });
  
  it("Should retrieve a user correctly", async () => {
    const mock_user = mock_data[0];
    await User.findOne({email: mock_user.email}).then((user) => {
      expect(user).not.toBeNull();
      expect(user).toBeInstanceOf(User);
    })
  });
  
  it("Should delete an existing user", async() => {
    const mock_user = mock_data[0];
    await User.deleteOne({email: mock_user.email}).then(res => {
      expect(res.deletedCount).toBeGreaterThan(0);
    });
  });
  
  it('Should throw validation errors', () => {
    const user = new User();
    expect(user.validate).toThrow();
  });
});
