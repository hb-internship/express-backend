export default [
    { name: "snack" },
    { name: "beverage" },
    { name: "dish" },
    { name: "side" },
    { name: "breakfast" },
    { name: "misc" }
]
