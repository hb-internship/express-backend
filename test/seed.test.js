import mongoose from "mongoose";
import User from '../build/schemes/user.model';
import Product from '../build/schemes/product.model';
import Category from '../build/schemes/category.model';

import mockUsers from "./mockUsers";
import mockProducts from "./mockProducts";
import mockCategories from "./mockCategories";

jest.unmock("mongoose");

jest.setTimeout(60000);

const connectToDB = async () => {
    await mongoose.connect("mongodb://localhost/hb-bistro-test", {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
};

describe('Populate user model', () => {
    beforeAll(async () => {
        await connectToDB();
        await mongoose.connection.db.dropCollection("users");
    });
    
    it("Should support the creation of 150 different users", async () => {
        expect.assertions(1);
        try {
            for (let i = 0; i < mockUsers.length; i++) {
                await (new User(mockUsers[i])).save();
            }
        } catch (err) {
            console.error("Something went wrong: ", err);
        }
        const users = await User.find({});
        expect(users.length).toBe(mockUsers.length);
    });
    
    it("Should clear the database collection of users", async () => {
        await User.deleteMany({});
        const users = await User.find({});
        expect(users.length).toBe(0);
    });

});

describe("Populate products", () => {
    beforeAll(async () => {
        await connectToDB();
        await mongoose.connection.db.dropCollection("products");
    });
    
    it("Should be able to create a new product", async () => {
        const productsCountOld = (await Product.find({})).length;
        const newProduct = new Product(mockProducts[0]);
        await newProduct.save();
        const productsCount = (await Product.find({})).length;
        expect(productsCount - productsCountOld).toBeGreaterThan(0);
    });
    
    it("Should be able to add multiple products", async () => {
        const productsOld = (await Product.find({})).length;
        for (let i = 0; i < mockProducts.length; i++) {
            const newProduct = new Product(mockProducts[i]);
            await newProduct.save();
        }
        const products = await Product.find({});
        expect(products.length - productsOld).toBe(mockProducts.length);
    });
});

describe("Populate categories", () => {
    beforeAll(async () => {
        await connectToDB();
        await mongoose.connection.db.dropCollection("categories");
    });
    
    it("Should be able to create a new category", async () => {
        const oldCount = (await Category.find({})).length;
        const newCategory = new Category(mockCategories[0]);
        await newCategory.save();
        const count = (await Category.find({})).length;
        expect(count - oldCount).toBeGreaterThan(0);
    });
    
    it("Should be able to add multiple categories", async () => {
        const oldCount = (await Category.find({})).length;
        for (let i = 0; i < mockCategories.length; i++) {
            const newCategory = new Category(mockCategories[i]);
            await newCategory.save();
        }
        const count = (await Category.find({})).length;
        expect(count - oldCount).toBe(mockCategories.length);
    })
});

describe("Categories linkage", () => {
    beforeAll(async () => {
        await connectToDB();
    });

    it("Should link a category to a product", async() => {
        const category = await Category.findOne({ name: mockCategories[0].name });
        const product = await Product.findOne({ name: mockProducts[0].name });
        product.categories.addToSet(category._id);
        await product.save();
        await Product.find({ categories: category._id }).populate("categories").then(console.log);
        
    });
    
    it("Should link multiple categories to the same product", async() => {
        const product = await Product.findOne({ name: mockProducts[0].name });
        for (let i = 0; i < mockCategories.length; i++) {
            const category = await Category.findOne({ name: mockCategories[i].name });
            product.categories.addToSet(category._id);
        }
        await product.save();
        await Product.findOne({ name: mockProducts[0].name }).populate("categories").then(console.log);
    });
});
