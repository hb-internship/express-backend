export default [
    { cost: 2.10, imagePath: null, name: "Coca Cola 33cl"},
    { cost: 1.50, imagePath: null, name: "Espresso"},
    { cost: 2.50, imagePath: null, name: "Fresh Orange Juice"},
    { cost: 1.30, imagePath: null, name: "Tea Cup"},
    { cost: 2.50, imagePath: null, name: "Cookie"},
    { cost: 2.00, imagePath: null, name: "Mars"},
    { cost: 1.80, imagePath: null, name: "Snack Bar"},
    { cost: 3.00, imagePath: null, name: "Chocolate"},
    { cost: 3.00, imagePath: null, name: "Pie of the day"},
    { cost: 2.50, imagePath: null, name: "Fruit"},
    { cost: 10.00, imagePath: null, name: "Main dish"},
    { cost: 8.00, imagePath: null, name: "Pasta"},
    { cost: 9.00, imagePath: null, name: "Big salad"},
    { cost: 4.00, imagePath: null, name: "Side salad"},
    { cost: 4.00, imagePath: null, name: "Side salad"},
    { cost: 6.00, imagePath: null, name: "Side dish"}
]
